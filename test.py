import main 
import pytest
import pandas as pd

dct = {}


def test_storage_item():
	item = main.StorageItem()
	assert item.df.empty

def test_making_report():
	for i in range(1,45):
		if i not in dct:
			dct[i] = main.StorageItem()
	dct[1].df = pd.DataFrame({'1' : [4,5,6,7], '2' : [10,20,30,40],'3' : [100,50,-30,-50]});
	assert main.MakingReport("2010-05-05", dct, ["2010-05-05"]).get_report() == "2010-05-05"


def test_type_error():
	with pytest.raises(TypeError):
		main.data_analysis(22)

def test_correct_length_data():
	assert len(dct[1].df["1"]) == 4

def test_daysrange():
	assert type(main.daysrange()) == list

def test_daysrange_len():
	assert len(main.daysrange()) == 20

def test_daysrange_rand():
	assert main.daysrange()[2] == '2010-05-03'

