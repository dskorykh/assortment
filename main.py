#coding: utf-8
"""
	структура программы:
		- получение списка дат
		- формирование из исходных файлов словаря с таблицами для каждого склада по всем датам
		- создание отчета
"""
import pandas as pd
import xlsxwriter as xl

class StorageItem(object):
	""" класс, который содержит данные по наполненности на складе каждой группы товаров
		self.df - инициализируется DataFrame для работы со складом	"""
	def __init__(self):
		self.df = pd.DataFrame({'A' : [],'B' : [],'C' : [],'D' : []})

class MakingReport(object):
	""" класс для создания отчета за определенную дату
	dct - словарь с датафреймами для каждой даты и склада
	date - дата, для которой делается отчет
	daterange - диапазон дат 
	"""
	def __init__(self, date, dct, daterange):
		self.date = date
		self.dct = dct
		self.daterange = daterange

	def get_report(self):
		"""метод для записи DataFrame в excel-файл для определенной даты 
		"""
		writer = pd.ExcelWriter('reports/report {}.xlsx'.format(self.date), engine='xlsxwriter')
		categories  = ['A', 'B', 'C', 'D']
		# создание объекта-книги
		workbook  = writer.book

		for i in range(1,45):
			# создание имени листа
			sheetname = "Storage {}".format(str(i))

			# выбор данных для нужной нам даты
			self.dct[i].df.filter(items=self.daterange[:int(self.date[-2:])]).to_excel(writer, sheet_name=sheetname)

			# создание  листа	
			worksheet = writer.sheets[sheetname]

			#добавление графика
			chart = workbook.add_chart({'type': 'line'})

			# заполнение данных для графика
			for i in range(len(categories)):
			    col = i + 1
			    chart.add_series({
			        'name':       [sheetname, col, 0],
			        'categories': [sheetname, 0, 1, 0 , int(self.date[-2:])],
			        'values':     [sheetname, col, 1, col, int(self.date[-2:])],
			    })

			# конфигурация осей графика
			chart.set_title({'name': sheetname})
			chart.set_x_axis({'name': 'Дата', 'major_gridlines': {'visible': True}})
			chart.set_y_axis({'name': 'Процент', 'major_gridlines': {'visible': True}})
			chart.set_size({'width': 1520, 'height': 776})
			# добавление графика на лист 
			worksheet.insert_chart('A1', chart)
		# сохранение и закрытие файла 
		writer.save()
		return self.date
		
def daysrange():
	""" создание списка с датами для дальнейшего использования с файлами excel"""
	daysarray = []
	# объект с датами, которые фигурируют в файлах
	rng = pd.date_range('2010-5-1', periods=20, freq='D')
	for day in rng:
		# получение значения даты в нужном нам формате и добавление ее в список
		daysarray.append(str(day)[:10])
	# возвращаем список с датами
	return daysarray

def data_analysis(dates):
	"""функция анализа файлов и получения из них информации о наполненности складов
	dates - список дат, для которых необходимо получить данные
	"""
	if type(dates) is not list:
		raise TypeError("type must be list")
	# создаем словарь для складов
	stor_dict = {}

	# цикл по датам в требуемом нам диапазоне
	for day in dates:

		# выгрузка таблицы в DataFrame table 
		table = pd.read_csv("ost/{}.csv".format(day), ";")

		# подсчет общего кол-ва товаров каждого класса на складе
		count_all_cls = table.groupby(['Class'])['Class'].count() 

		for i in range(1,45):
			# для каждого склада создаем новый объект и помещаем его в словарь stor_dict с ключом = номером склада
			if i not in stor_dict:
				stor_dict[i] = StorageItem()

			# выборка из общей таблицы позиций товаров всех классов для каждого склада с ненулевым остатком
			stor = table[table["Storage {}".format(str(i))] > 0][["Class", "Storage {}".format(str(i))]] 

			# подсчет остатка товаров каждого класса на складе
			count_stor_cls = stor.groupby(["Class"])['Class'].count() 

			# процентное соотношение остатка к общему кол-ву
			perc = count_stor_cls/count_all_cls*100 
			perc.name = day
			# проверка на заполненность DataFrame данными и последовательное добавление данных из таблиц по каждому складу
			if stor_dict[i].df.empty:
				stor_dict[i].df = pd.DataFrame(perc)
			else:
				tempdf = pd.DataFrame(perc)
				frames = [stor_dict[i].df, tempdf]
				stor_dict[i].df = pd.concat(frames, axis=1, join='inner')
	return stor_dict

def make_all_reports(dct, daterange, first_rep, last_rep):
	""" функция для создания отчетов в указанном диапазоне"""
	# оставляем только те даты, для которых нам нужен отчет
	daterange_ = daterange[daterange.index(first_rep):daterange.index(last_rep)+1]
	for day in daterange_:
		MakingReport(day, dct, daterange).get_report()

# получаем список дат, за которые у нас есть отчеты
date_range = daysrange()

# формируем словарь с данными по всем складам за всё время
stor_dict = data_analysis(date_range)

# создаем отчеты для нужного нам диапазона дат
make_all_reports(stor_dict, date_range, "2010-05-05", "2010-05-20")

